include <colors.scad>
include <sssystem.scad>

w1 = 1100;  // width
h = 700;   // heigth
d1 = 550;   // depth
t = 24;    // thickness

w2 = 1050;
d2 = 250;

w3 = 700;

lw = 60;   // leg width

////////////////////////////////////////
// left legs
module leg(w = 50, h = 676, d = 500, t = 24, c = teak) {
    // front
    color(c)
    cube([w, t, h]);

    // front back
    color(c) translate([0, t, t])
    cube([w, t, h - 2 * t]);

    // rear back
    color(c) translate([0, d - t, 0])
    cube([w, t, h]);

    // rear
    color(c) translate([0, d - 2 * t, t])
    cube([w, t, h - 2 * t]);

    // bottom
    color(c) translate([0, t, 0])
    cube([w, d - 2 * t, t]);

    // bottom
    color(c) translate([0, 2*t, t])
    cube([w, d - 4 * t, t]);

    // top
    color(c) translate([0, t, h - 1 * t])
    cube([w, d - 2 * t, t]);
}

module leg2(w = 50, h = 676, d = 500, t = 24, c = teak) {
    color(c)
    cube([w, t, h]);

    color(c)
    translate([0, d - t, 0])
    cube([w, t, h]);

    color(c)
    translate([0, t, 0])
    cube([w, d - 1 * t, t]);

    color(c)
    translate([0, t, h - 1 * t])
    cube([w, d - 2 * t, t]);
}


////////////////////////////////////////
// text
translate([w1 / 2, -100, h])
color("black")
text(str(w1), size = 30);

translate([-30, d1 / 2, h])
rotate([0,0,90])
color("black")
text(str(d1), size = 30);


translate([w1 / 2, 0, h + t])
rotate([90,0,0])
color("black")
text(str(w1), size = 30);

translate([-t, 0, h /2])
rotate([0,270,90])
color("black")
text(str(h), size = 30);



////////////////////////////////////////
// Desk

wh = 100; // cable hole width
dh = 25;  // cable hole depth
ph = 200; // cable hole position

// top board
color(teak) translate([0, 0, h - t])
difference() {
    cube([w1, d1, t]);
    translate([ph, d1-dh, -1]) cube([wh, dh+1, t+2]);
    translate([w1-ph-wh, d1-dh, -1]) cube([wh, dh+1, t+2]);
}

// left leg
translate([5, 5, 0]) leg(w=lw, d=d1-10);

// right leg
translate([w1 - lw - 5, 5, 0]) leg(w=lw, d=d1-10);



////////////////////////////////////////
// shelves

// right leg
translate([d2-5, -w2+5, 0]) rotate([0, 0, 90])
    leg2(w=lw, d=d2-10);
// left leg
translate([d2-5, -w2+w3-lw, 0]) rotate([0, 0, 90])
    leg2(w=lw, d=d2-10);

// top board
color(teak) translate([0, -w2, h - t])
cube([d2, w2, t]);

// 2nd board
color(teak)
translate([t, -w2+15, 420-t])
cube([d2-2*t, w3-20, t]);

//// 3rd board
//color(teak)
//translate([t, -w2+15, 500-t])
//cube([d2-2*t, w3-20, t]);

// base board
color(teak)
translate([t, -w2+15, 100-t])
cube([d2-2*t, w3-20, t]);


////////////////////////////////////////////////////////////////////////////////
// Shitaji and Brackets
////////////////////////////////////////////////////////////////////////////////

sh1 = 300; // shelf height 1
sh2 = 350; // shelf height 2
sh3 = 350;

s1 = -d1 + 380;
s2 = -d1 + 820;
s3 = -d1 + 1300;
s4 = -d1 + 1740;

h1 = 1450;
h0 = h1 - 250 - t;
h2 = h1 + sh1 + t;
h3 = h1 + sh1 + sh2 + 2*t;
h4 = h1 + sh1 + sh2 + sh3 + 3*t;
// 1400 + 250 + 300 + 350 + 96 = 2,396

bt120 = "sugatsune/bt-120.stl";
bt240 = "sugatsune/bt-240.stl";
btk120 = "sugatsune/btk-ub-120_0.stl";

/*
rotate([90, 0, -90]) translate([s1, h1, 0]) color("black") import(bt240);
rotate([90, 0, -90]) translate([s2, h1, 0]) color("black") import(bt240);
rotate([90, 0, -90]) translate([s3, h1, 0]) color("black") import(bt240);

rotate([90, 0, -90]) translate([s1, h2, 0]) color("black") import(bt240);
rotate([90, 0, -90]) translate([s2, h2, 0]) color("black") import(bt240);
rotate([90, 0, -90]) translate([s3, h2, 0]) color("black") import(bt240);
*/


rotation = [90, 0, 0];
rotate([90, 0, -90]) translate([s1, h0, 0]) color("black") import(bt120);
rotate([90, 0, -90]) translate([s2, h0, 0]) color("black") import(bt120);
rotate([90, 0, -90]) translate([s3, h0, 0]) color("black") import(bt120);

rotate(rotation) translate([0, h1, s1]) color("black") import(btk120);
rotate(rotation) translate([0, h1, s2]) color("black") import(btk120);
rotate(rotation) translate([0, h1, s3]) color("black") import(btk120);

rotate(rotation) translate([0, h2, s1]) color("black") import(btk120);
rotate(rotation) translate([0, h2, s2]) color("black") import(btk120);
rotate(rotation) translate([0, h2, s3]) color("black") import(btk120);

rotate(rotation) translate([0, h3, s1]) color("black") import(btk120);
rotate(rotation) translate([0, h3, s2]) color("black") import(btk120);
rotate(rotation) translate([0, h3, s3]) color("black") import(btk120);

// rotate(rotation) translate([0, h4, s1]) color("black") import(btk120);
// rotate(rotation) translate([0, h4, s2]) color("black") import(btk120);
// rotate(rotation) translate([0, h4, s3]) color("black") import(btk120);


// Front Bracket
rotate([90, 0, -90]) translate([-d1, h1, -400]) color("black") import(btk120);
rotate([90, 0, -90]) translate([-d1, h1, -1300]) color("black") import(btk120);

rotate([90, 0, 180]) translate([-400, h0, d1]) color("black") import(bt120);
rotate([90, 0, 180]) translate([-1300, h0, d1]) color("black") import(bt120);


rotate([0, 0, -90]) translate([s1, -11, 0]) ss2_ssf_11();

rotate([0, 0, -90]) translate([s2, -11, 0]) ss2_ssf_11();

rotate([0, 0, -90]) translate([s3, -11, 0]) ss2_ssf_11();

rotate([0, 0, -90]) translate([s4, -11, 0]) ss2_ssf_11();


////////////////////////////////////////////////////////////////////////////////
// Shelves
////////////////////////////////////////////////////////////////////////////////

color(teak) translate([0, -w2, h0]) cube([150, w2+d1, t]);
color(teak) translate([0, -w2, h1]) cube([d2, w2+d1, t]);
color(teak) translate([0, -w2, h2]) cube([d2, w2+d1, t]);
color(teak) translate([0, -w2, h3]) cube([d2+20, w2+d1, t]);
// color(teak) translate([0, -w2, h4]) cube([d2, w1+d1, t]);

// Front boards
color(teak) translate([0, d1-d2, h1]) cube([1400, d2, t]);
d3 = 130;
color(teak) translate([0, d1-d3, h0]) cube([1400, d3, t]);

////////////////////////////////////////////////////////////////////////////////
// Window
////////////////////////////////////////////////////////////////////////////////

color("white") translate([455, d1, 1555]) cube([1155, 100, 540]);
