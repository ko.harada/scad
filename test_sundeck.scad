include <sunself.scad>

w = 600;
w2 = 100;
h = 400;
d = 300;

translate([sw03_height,0,0]) rotate([0, 270, 0]) sw03(h - sw03_height);
translate([-sw03_height, -110, 0]) rotate([90, 0, 90]) sw03(d);

translate([w - 2*w2, 0, 0]) rotate([0, 270, 0]) sw03(h - sw03_height);
translate([w - 2*w2, -110, 0]) rotate([90, 0, 90]) sw03(300);

translate([-w2, 0, h - sw03_height]) sw03(w);
