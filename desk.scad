w = 1100;  // width
h = 700;   // heigth
d = 500;   // depth
t = 24;    // thickness

lw = 50;   // leg width

import("../cad/bt-480.dxf");

////////////////////////////////////////
// text
translate([w / 2, -100, h])
color("black")
text(str(w), size = 30);

translate([-30, d / 2, h])
rotate([0,0,90])
color("black")
text(str(d), size = 30);


translate([w / 2, 0, h + t])
rotate([90,0,0])
color("black")
text(str(w), size = 30);

translate([-t, 0, h /2])
rotate([0,270,90])
color("black")
text(str(h), size = 30);



////////////////////////////////////////
// top board
translate([0, 0, h - t])
cube([w, d, t]);

////////////////////////////////////////
// left legs
cube([lw, t, h - t]);
translate([0, t, t])
cube([lw, t, h - 3 * t]);

translate([0, d - t, 0])
cube([lw, t, h - t]);
translate([0, d - 2 * t, t])
cube([lw, t, h - 3 * t]);

translate([0, t, 0])
cube([lw, d - 2 * t, t]);

translate([0, t, h - 2 * t])
cube([lw, d - 2 * t, t]);

////////////////////////////////////////
// right legs
translate([w - lw, 0, 0])
cube([lw, t, h - t]);
translate([w - lw, t, t])
cube([lw, t, h - 3 * t]);

translate([w - lw, d - t, 0])
cube([lw, t, h - t]);
translate([w - lw, d - 2 * t, t])
cube([lw, t, h - 3 * t]);

translate([w - lw, t, 0])
cube([lw, d - 2 * t, t]);

translate([w - lw, t, h - 2 * t])
cube([lw, d - 2 * t, t]);

