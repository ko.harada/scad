
module pipe(h = 1000, r = 32, thickness = 2, center = false) {
    difference() {
        cylinder(h = h, r = r/2, center = center);
        cylinder(h = h, r = r/2 - thickness, center = center);
    }
}

module bracket_L(r = 25, center = true) {
    t = 2; // thickness
    translate([0, 0, 3 * r]) rotate([90, 0, 90]) cylinder(h = 10, r = r/2 + t, center = center);
    translate([0, 0, 1.5*r]) cube([10, r + 2*t, 3 * r], center = center);
    translate([0, 0, t]) cube([10, 2 * r, 2 * t], center = center);
}

translate([100, 0, 0]) pipe();
bracket_L();


