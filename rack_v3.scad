include <sssystem.scad>;
use <hanger.scad>;
include <colors.scad>;

////////////////////////////////////////////////////////////////////////////////
// Defs
////////////////////////////////////////////////////////////////////////////////
t = 24; // thickness

////////////////////////////////////////////////////////////////////////////////
// Duct and Outlet
////////////////////////////////////////////////////////////////////////////////

// Duct
color(ss2_ivory) translate([0, -800, 1800]) cube([25, 130, 130]);

// Outlet
color(ss2_ivory) translate([0, -100, 1780]) cube([10, 70, 120]);


////////////////////////////////////////////////////////////////////////////////
// supports
////////////////////////////////////////////////////////////////////////////////

s1 = 380;
s2 = 820;
s3 = 1300;
s4 = 1740;

translate([s1, 0, 250]) ss2_ssf_11();

translate([s2, 0, 250]) ss2_ssf_11();

translate([s3, 0, 250]) ss2_ssf_11();

translate([s4, 0, 250]) ss2_ssf_11();

////////////////////////////////////////////////////////////////////////////////
// Walls
////////////////////////////////////////////////////////////////////////////////

// color("#f9f9f9") cube([3000, 1, 2400]);

////////////////////////////////////////////////////////////////////////////////
// Shelves
////////////////////////////////////////////////////////////////////////////////

color(teak) translate([0, -440, 1000]) cube([1800, 440, 24]);
    translate([s1+3, -400, 1000 -t]) ss2_wob();
    translate([s2+3, -400, 1000 -t]) ss2_wob();
    translate([s3+3, -400, 1000 -t]) ss2_wob();
    translate([s4+3, -400, 1000 -t]) ss2_wob();

color(teak) translate([0, -440, 660]) cube([900, 440, 24]);
    translate([s1+3, -400, 660 -t]) ss2_wob();
    translate([s2+3, -400, 660 -t]) ss2_wob();

color(teak) translate([0, -440, 330]) cube([900, 440, 24]);
    translate([s1+3, -400, 340 -t]) ss2_wob();
    translate([s2+3, -400, 340 -t]) ss2_wob();

color(teak) translate([100, -440, 2000]) cube([1700, 440, 24]);
    translate([s1+3, -400, 2000 -t]) ss2_wob();
    translate([s2+3, -400, 2000 -t]) ss2_wob();
    translate([s3+3, -400, 2000 -t]) ss2_wob();
    translate([s4+3, -400, 2000 -t]) ss2_wob();

color(teak) translate([0, -440, 1330]) cube([900, 440, 24]);
    translate([s1+3, -400, 1330 -t]) ss2_wob();
    translate([s2+3, -400, 1330 -t]) ss2_wob();

color(teak) translate([0, -440, 1660]) cube([900, 440, 24]);
    translate([s1+3, -400, 1660 -t]) ss2_wob();
    translate([s2+3, -400, 1660 -t]) ss2_wob();

////////////////////////////////////////////////////////////////////////////////
// Hangers
////////////////////////////////////////////////////////////////////////////////

color(ss2_silver) translate([1325, -250, 1000 - 3*25]) rotate([0, 90, 0]) pipe(h = 910, r = 25, center = true);
color(ss2_silver) translate([1350, -250, 2000 - 3*25]) rotate([0, 90, 0]) pipe(h = 900, r = 25, center = true);

color(ss2_silver) translate([1350, -250, 1000]) rotate([180, 0, 0]) bracket_L(r = 25);
color(ss2_silver) translate([1770, -250, 1000]) rotate([180, 0, 0]) bracket_L(r = 25);
color(ss2_silver) translate([900, -250, 1000]) rotate([180, 0, 0]) bracket_L(r = 25);

color(ss2_silver) translate([1350, -250, 2000]) rotate([180, 0, 0]) bracket_L(r = 25);
color(ss2_silver) translate([1770, -250, 2000]) rotate([180, 0, 0]) bracket_L(r = 25);
color(ss2_silver) translate([900, -250, 2000]) rotate([180, 0, 0]) bracket_L(r = 25);
