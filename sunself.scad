sw03_width = 80;
sw03_height = 35;

module sw03(len, col="#5d4037") {
    w = sw03_width; // width
    h = sw03_height; // height
    t = 4;  // thickness
    wi = (w - 4*t)/3; // inner width
    hi = h - 2*t; // inner height
    color(col)
    difference() {
        cube([len, w, h]);
        translate([-1, t, t]) cube([len+2, wi, hi]);
        translate([-1, 2*t + wi, t]) cube([len+2, wi, hi]);
        translate([-1, 3*t + 2*wi, t]) cube([len+2, wi, hi]);
    }
}

