////////////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////////////
ss2_ivory = "#fffeee";
ss2_silver = "#c4cace";
ss2_grey = "#3d4849";

module ss2_ssf_11(size = 1820, color = ss2_ivory) {
    if (color == "ivory") {
        color = ss2_ivory;
    }
    if (color == "silver") {
        color = ss2_silver;
    }
    if (color == "grey") {
        color = ss2_grey;
    }
    w = 8.1;
    d = 11;
    h = size;
    color(color) cube([w, d, h]);
}

module ss2_wob(size = 400, color = ss2_grey) {
    if (color == "ivory") {
        color = ss2_ivory;
    }
    if (color == "silver") {
        color = ss2_silver;
    }
    if (color == "grey") {
        color = ss2_grey;
    }
    t = 3;
    h1 = 20;
    h2 = 30;
    color(color) cube([t, size, h1]);
}


